/**
 * File: src/distEA/distdistEAAnalysis.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 02/05/15		hcai		created; for computing method-level impact sets according to distEA sequences
 * 02/19/15		hcai		done code drafting; simply do trace merging and existing EAS based impact computation 
 * 02/24/15		hcai		separately report impacts occurred in the process where the query is executed from those in other processes
 * 03/18/15		hcai		one more data item reported: ratio of impact set size to the size of covered set
 * 03/23/15		hcai		added one more data item in the report: aggregate result per query for all execution traces used
 * 08/07/15		hcai		fixed results reporting: by default, local and external impact sets include common impacts when computing the 
 * 							size ratios of local (remote) distEA impact set to local (remote) covered set of methods
 * 08/13/15		hcai		implement the precision improvement by leveraging message-passing semantics: a method m in Pj (process i) can
 * 							be impacted by the query c in Pi only if max(last return event time of m, last returned-into event time of m) >=
 * 							min(first entry event time of c, time of first message received from Pi)     
*/
package distEA;
import java.io.*;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class distEAAnalysis{
	static Set<String> changeSet = new LinkedHashSet<String>();
	static Set<String> impactSet = new LinkedHashSet<String>();
	static int nExecutions = Integer.MAX_VALUE;
	
	static boolean debugOut = false;
	static boolean separateReport = true;
	static boolean reportCommon = true;
	static boolean strictComponent = false; // strictly two different components won't have common traces --- they have to communicate by message passing
	static boolean improvedPrec = false; // choose between the purely EA-based and precision-improved versions
	
	public static void main(String args[]) {
		if (args.length < 2) {
			System.err.println("Too few arguments: \n\t " +
					"distEAAnalysis changedMethods/queryList traceDir [numberTraces]\n\n");
			return;
		}
		
		String changedMethods = args[0]; // tell the changed methods, separated by comma if there are more than one
		String traceDir = args[1]; // tell the directory where execution traces can be accessed
		
		// read at most N execution traces if specified, otherwise exhaust all to be found
		if (args.length > 2) {
			nExecutions = Integer.parseInt(args[2]);
		}
		
		if (args.length > 3) {
			separateReport = args[3].startsWith("-separate");
		}
		if (args.length > 4) {
			reportCommon  = args[4].startsWith("-common");
		}
		if (args.length > 5) {
			strictComponent  = args[5].startsWith("-strict");
		}
		if (args.length > 6) {
			improvedPrec  = args[6].startsWith("-prec");
		}
		if (args.length > 7) {
			debugOut = args[7].equalsIgnoreCase("-debug");
		}
		
		if (debugOut) {
			System.out.println("Try to read [" + (-1==nExecutions?"All available":nExecutions) + "] traces in " 
					+ traceDir + " with changed methods being " + changedMethods);
		}
		
		try {
			// process the given query directly
			File funclist = new File(changedMethods);
			if (!funclist.isFile()) {
				if (separateReport) {
					System.out.println("separateReport");
					separateParseTraces(changedMethods, traceDir);
				}
				else {
					System.out.println("Not separateReport");
					startParseTraces(changedMethods, traceDir);
					printStatistics(impactSet, true);
				}
				return;
			}
			
			// query each function as a candidate change method in the given list
			BufferedReader br;
			br = new BufferedReader(new InputStreamReader(new FileInputStream(funclist)));
			String ts = br.readLine();
			while(ts != null) {
				if (separateReport) {
					changeSet.clear();
					impactSet.clear();
					separateParseTraces(ts.trim(), traceDir);
				}
				else {
					long queryCost = System.currentTimeMillis();
					changeSet.clear();
					impactSet.clear();
					int sz_covered = startParseTraces(ts.trim(), traceDir);
					queryCost = System.currentTimeMillis() - queryCost;
					printStatistics(impactSet, true);
					// summary report
					if (!changeSet.isEmpty() && !impactSet.isEmpty()) {
						int issize = impactSet.size();
						DecimalFormat df = new DecimalFormat("#.####");
						System.out.println(changeSet.toArray()[0] + "\t" + issize + "\t" + sz_covered + "\t" + 
								df.format(issize*1.0/sz_covered) + "\t" + queryCost);
					}
				}
				
				ts = br.readLine();
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void mergeTraces(File loc, HashMap<String, Integer> F, HashMap<String, Integer> L)  throws IOException, ClassNotFoundException {
		if (loc.isFile() && loc.getName().endsWith(".em")) {
			FileInputStream fis = new FileInputStream(loc);
			ObjectInputStream ois = new ObjectInputStream(fis);
			HashMap<String, Integer> curF =  (HashMap<String, Integer>) ois.readObject();
			HashMap<String, Integer> curL =  (HashMap<String, Integer>) ois.readObject();
			
			F.putAll(curF);
			L.putAll(curL);
			
			fis.close();
			ois.close();
			
			return;
		}
		
		if (!loc.isDirectory()) { return; }
		
		for (String fname : loc.list()) {
			mergeTraces (new File(loc, fname), F, L);
		}
	}
	
	
	public static int startParseTraces(String changedMethods, String traceDir) {
		int tId;
		List<String> Chglist = dua.util.Util.parseStringList(changedMethods, ';');
		if (Chglist.size() < 1) {
			// nothing to do
			return 0;
		}
		
		Set<String> coveredSet = new HashSet<String> ( );
		
		for (tId = 1; tId <= nExecutions; ++tId) {
			String CLT = "";
			Integer tsCLT = Integer.MAX_VALUE;
			
			try {
				String dnSource = traceDir  + File.separator + "test" + tId + File.separator;
				HashMap<String, Integer> F = new HashMap<String, Integer> ( );
				HashMap<String, Integer> L = new HashMap<String, Integer> ( );
				
				if (debugOut) {
					System.out.println("\nDeserializing event maps from " + dnSource);
				}
				
				// 1. reconstruct the two event maps from the serialized execution trace associated with this test
				mergeTraces(new File(dnSource), F, L);
				
				coveredSet.addAll(F.keySet());
				coveredSet.addAll(L.keySet());
				
				// -- DEBUG
				if (debugOut) {
					System.out.println("\n[ First events ]\n" + F );
					System.out.println("\n[ Last events ]\n" + L );
				}
				
				// 2. determine the CLT (Change with Least Time-stamp in F) 
				List<String> localChgSet = new ArrayList<String>();
				for (String chg : Chglist) {
					for (String m : F.keySet()) {
						if ( !m.toLowerCase().contains(chg.toLowerCase()) && 
								!chg.toLowerCase().contains(m.toLowerCase()) ) {
							// unmatched change specified even with a very loose matching
							continue;
						}
						localChgSet.add(m);
						if (F.get(m) <= tsCLT) {
							tsCLT = F.get(m);
							CLT = m;
						}
					}
				}
				if (localChgSet.isEmpty()) {
					// nothing to do with this execution trace with respect to the given change set
					continue;
				}
				changeSet.addAll(localChgSet);
				
				// 3. compute the impact set with respect to this execution trace
				Set<String> localImpactSet = new LinkedHashSet<String>();
				for (String m : L.keySet()) {
					if (L.get(m) >= tsCLT) {
						localImpactSet.add(m);
					}
				}
				if (debugOut) {
					System.out.println("\n============ EAS result from current trace [no. " + tId + "] ================");
					System.out.println("CLT: " + CLT + "[ts=" + tsCLT + "]\n");
					printStatistics(localImpactSet, false);
				}
				// --
				impactSet.addAll(localImpactSet);
			}
			catch (FileNotFoundException e) { 
				break;
			}
			catch (ClassCastException e) {
				System.err.println("Failed to cast the object deserialized to HashMap<String, Integer>!");
				return 0;
			}
			catch (IOException e) {
				throw new RuntimeException(e); 
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println(--tId + " execution traces have been processed.");
		return coveredSet.size();
	}

	private static void printStatistics (Set<String> impactset, boolean btitle) {
		if (btitle) {
			System.out.println("\n============ Final distEA Result ================");
			System.out.println("[Valid Change Set]");
			for (String m:changeSet) {
				System.out.println(m);
			}
		}
		
		System.out.println("[Change Impact Set]: size= " + impactset.size());
		for (String m:impactset) {
			System.out.println(m);
		}
	}
	
	
	/** compute impacts from the trace dumped by one OS process with respect to a test input */ 
	private static int computeProcesTrace(List<String> Chglist, HashMap<String, Integer> F, 
			HashMap<String, Integer> L, List<String> localChgSet, Set<String> localImpactSet) {
		// determine the CLT (Change with Least Time-stamp in F)
		String CLT = "";
		Integer tsCLT = Integer.MAX_VALUE;
		for (String chg : Chglist) {
			for (String m : F.keySet()) {
				if ( !m.toLowerCase().contains(chg.toLowerCase()) && 
						!chg.toLowerCase().contains(m.toLowerCase()) ) {
					// unmatched change specified even with a very loose matching
					continue;
				}
				localChgSet.add(m);
				if (F.get(m) <= tsCLT) {
					tsCLT = F.get(m);
					CLT = m;
				}
			}
		}
		// compute the impact set with respect to this execution trace
		for (String m : L.keySet()) {
			if (L.get(m) >= tsCLT) {
				localImpactSet.add(m);
			}
		}
		return tsCLT;
	}
	public static void separateParseTraces(String changedMethods, String traceDir) {
		int tId;
		List<String> Chglist = dua.util.Util.parseStringList(changedMethods, ';');
		if (Chglist.size() < 1) {
			// nothing to do
			return;
		}
		
		long tszLocal = 0, tszExternal = 0, tszCommon = 0, tszExCov = 0, tszLocCov = 0, tszCov = 0, tqueryCost = 0;
		Set<String> allCovered = new HashSet<String> ( );
		Set<String> allExCov = new HashSet<String> ( );
		Set<String> allLocCov = new HashSet<String> ( );
		
		Set<String> allLocIS = new HashSet<String> ( );
		Set<String> allExIS = new HashSet<String> ( );
		Set<String> allComIS = new HashSet<String> ( );
		
		for (tId = 1; tId <= nExecutions; ++tId) {
			try {
				String dnSource = traceDir  + File.separator + "test" + tId + File.separator;
				Map<String, HashMap<String, Integer>> f2L = new HashMap<String, HashMap<String, Integer>> ( );
				Map<String, HashMap<String, Integer>> f2F = new HashMap<String, HashMap<String, Integer>> ( );
				Map<String, HashMap<String, Integer>> f2S = new HashMap<String, HashMap<String, Integer>>();
				
				Set<String> covered = new HashSet<String> ( );
								
				if (debugOut) {
					System.out.println("\nDeserializing event maps from " + dnSource);
				}
				
				File dn = new File(dnSource);
				assert dn.isDirectory();
				
				// load trace of all processes into memory
				long fixtime = System.currentTimeMillis();
				for (String fname : dn.list()) {
					File loc = new File(dn, fname);
					if (loc.isFile() && loc.getName().endsWith(".em")) {
						FileInputStream fis = new FileInputStream(loc);
						ObjectInputStream ois = new ObjectInputStream(fis);
						HashMap<String, Integer> curF =  (HashMap<String, Integer>) ois.readObject();
						HashMap<String, Integer> curL =  (HashMap<String, Integer>) ois.readObject();
						//**************************** printf curF, curL's method timestamp
						//if (improvedPrec) 
						{
							HashMap<String, Integer> _curS =  (HashMap<String, Integer>) ois.readObject();
							HashMap<String, Integer> curS = new HashMap<String, Integer>();
							for (Map.Entry<String, Integer> entry : _curS.entrySet()) {
								String k = entry.getKey().trim().replace("\0", "");
								Integer v = entry.getValue();
								curS.put(k, v);
							}
							f2S.put(loc.getAbsolutePath(), curS);
						}
						
						f2L.put(loc.getAbsolutePath(), curL);
						f2F.put(loc.getAbsolutePath(), curF);
						
						covered.addAll(curF.keySet());
						covered.addAll(curL.keySet());
						
						fis.close();
						ois.close();
					}
				}
				if (improvedPrec && debugOut) {
					System.out.println("Content of Sender map:");
					for (String tr : f2S.keySet()) {
						System.out.println("in trace of " + tr);
						for (String sender : f2S.get(tr).keySet()) {
							System.out.println(sender + "\t" + f2S.get(tr).get(sender));
						}
					}
				}
				fixtime = System.currentTimeMillis() - fixtime;
				
				// compute impacts in local and external processes
				for (String tf : f2F.keySet()) {
					Set<String> locCovered = new LinkedHashSet<String>();
					Set<String> exCovered = new LinkedHashSet<String>();
					
					long queryCost = System.currentTimeMillis();
					
					HashMap<String, Integer> curF =  f2F.get(tf);
					HashMap<String, Integer> curL = f2L.get(tf);
					
					String ProcessI = null;
					//if (improvedPrec) 
					{
						HashMap<String, Integer> curS = f2S.get(tf);
						assert curS.containsValue(Integer.MAX_VALUE);
						for (String pid : curS.keySet()) {
							if (curS.get(pid)==Integer.MAX_VALUE) {
								ProcessI = pid;
								break;
							}
						}
						assert ProcessI != null;
					}
					
					List<String> localChgSet = new ArrayList<String>();
					Set<String> localImpactSet = new LinkedHashSet<String>();
					
					locCovered.addAll(curF.keySet());
					locCovered.addAll(curL.keySet());
					
					int tsCLT = computeProcesTrace(Chglist, curF, curL, localChgSet, localImpactSet);
					if (localChgSet.isEmpty()) {
						continue;
					}
					
					if (debugOut) {
						System.out.println("[Change Set ] " + localChgSet + " from " + tf);
					}
					
					if (debugOut)
					for (String m:localChgSet) {
						System.out.println(m);
					}
					
					changeSet.addAll(localChgSet);
					impactSet.addAll(localImpactSet);
										
					// impacted methods in other processes
					Set<String> exImpactSet = new LinkedHashSet<String>();
					int excludedN = 0;
					for (String tl : f2L.keySet()) {
						if (tl.equalsIgnoreCase(tf)) { continue; }
						if (strictComponent) {
							Set<String> lptrace = new LinkedHashSet<String>(f2F.get(tf).keySet());
							lptrace.retainAll(f2L.get(tl).keySet());
							if (!lptrace.isEmpty()) continue;
						}
						
						exCovered.addAll(f2L.get(tl).keySet());
						if (f2F.containsKey(tl)) {
							exCovered.addAll(f2F.get(tl).keySet());
						}
						
						
						for (String m : f2L.get(tl).keySet()) {
							if (f2L.get(tl).get(m) >= tsCLT) {
								if (improvedPrec) {
									HashMap<String, Integer> curS = f2S.get(tl);
									/*
									System.out.println("tl: " + tl + " content of curS: " + curS + " first msgRecv time from " + ProcessI + ": " + curS.get(ProcessI));
									for (String pid : curS.keySet()) {
										System.out.println("compare " + pid + " with " + ProcessI + ": " + ProcessI.equalsIgnoreCase(pid.trim()));
									}
									*/
									if (debugOut) {
										if (!curS.containsKey(ProcessI)) {
											System.out.println(ProcessI + " is NOT found in the sender map..., is that correct?");
											System.out.println(ProcessI + " is NOT in the set of " + curS.keySet());
										}
										else {
											System.out.println(ProcessI + " is INDEED found in the sender map, is that correct?");
										}
									}
									if (curS.containsKey(ProcessI) && f2L.get(tl).get(m)>=curS.get(ProcessI)) {
										exImpactSet.add(m);
									}
									else {
										excludedN ++;	
									}
								}
								else {
									exImpactSet.add(m);
								}
							}
						}
					}
					
					// report
					Set<String> commonSet = null;
					if (reportCommon) {
						commonSet = new HashSet<String>(localImpactSet);
						commonSet.retainAll(exImpactSet);
						
						localImpactSet.removeAll(commonSet);
						exImpactSet.removeAll(commonSet);
					
						if (debugOut)  {
							System.out.println("[common impact Set ] size = " + commonSet.size());
							for (String m:commonSet) {
								System.out.println(m);
							}
						}
					}
					
					if (debugOut) {
						System.out.println("[local impact Set ] size = " + localImpactSet.size());
						for (String m:localImpactSet) {
							System.out.println(m);
						}
					}
					
					if (debugOut)  {
						System.out.println("[external impact set ] size = " + exImpactSet.size());
						for (String m:exImpactSet) {
							System.out.println(m);
						}
						System.out.println(excludedN + " methods were excluded from external impact set due to the precision improvement.");
					}
					impactSet.addAll(exImpactSet);
					impactSet.addAll(commonSet);
					
					/*
					if (reportCommon) {
						localImpactSet.addAll(commonSet);
						exImpactSet.addAll(commonSet);
					}
					*/
					
					assert locCovered.containsAll(localImpactSet);
					assert exCovered.containsAll(exImpactSet);
					
					queryCost = System.currentTimeMillis() - queryCost + fixtime;
					// summary report 
					int issize = localImpactSet.size() + exImpactSet.size() + commonSet.size();
					DecimalFormat df = new DecimalFormat("#.####");
					System.out.println(ProcessI+"_"+localChgSet.get(0) + "\t" + issize + "\t" + localImpactSet.size() + "\t" + exImpactSet.size() +
							"\t" + commonSet.size() + "\t" + queryCost + "\t" + 
							df.format( (localImpactSet.size()+commonSet.size())*1.0/locCovered.size()) + "\t" +
							df.format( (exImpactSet.size()+commonSet.size())*1.0/exCovered.size()) + "\t" +
							df.format(issize*1.0/covered.size()));
					
					tszLocal += localImpactSet.size();
					tszExternal += exImpactSet.size(); 
					tszCommon += commonSet.size();
					tszExCov += exCovered.size();
					tszLocCov += locCovered.size();
					tszCov += covered.size();
					tqueryCost += queryCost;
					
					// another way to look at the aggregate results
					allCovered.addAll(covered);
					allExCov.addAll(exCovered);
					allLocCov.addAll(locCovered);
					
					allLocIS.addAll(localImpactSet);
					allExIS.addAll(exImpactSet);
					allLocIS.addAll(commonSet);
					allExIS.addAll(commonSet);
					allComIS.addAll(commonSet);
				}
								
				if (debugOut) {
					System.out.println("\n total covered methods = " + covered.size() + "\n");
					System.out.println("\n============ Done processing current trace [no. " + tId + "] ================");
				}
				// --
			}
			catch (FileNotFoundException e) {
				break;
			}
			catch (ClassCastException e) {
				System.err.println("Failed to cast the object deserialized to HashMap<String, Integer>!");
				return;
			}
			catch (IOException e) {
				throw new RuntimeException(e); 
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (debugOut) {
			System.out.println(--tId + " execution traces have been processed.");
		}
		if (tszLocal+tszExternal != 0) {
			DecimalFormat df = new DecimalFormat("#.####");
			System.out.println("[SummaryPerQuery]"+"\t"+changedMethods + "\t" + (tszLocal+tszExternal+tszCommon) + "\t" + tszLocal + "\t" + tszExternal +
					"\t" + tszCommon + "\t" + tqueryCost + "\t" + 
					df.format( (tszLocal+tszCommon)*1.0/tszLocCov) + "\t" +
					df.format( (tszExternal+tszCommon)*1.0/tszExCov) + "\t" +
					df.format((tszLocal+tszExternal+tszCommon)*1.0/tszCov));
			
			if (reportCommon) {
				allLocIS.removeAll(allComIS);
				allExIS.removeAll(allComIS);
			}
			System.out.println("[AggregatePerQuery]"+"\t"+changeSet + "\t" + (impactSet.size()) + "\t" + allLocIS.size() + 
					"\t" + allExIS.size() +
					"\t" + allComIS.size() + "\t" + tqueryCost + "\t" + 
					df.format((allLocIS.size()+ allComIS.size())*1.0/allLocCov.size()) + "\t" +
					df.format((allExIS.size()+ allComIS.size())*1.0/allExCov.size()) + "\t" +
					df.format((impactSet.size())*1.0/allCovered.size()));
		}
	}
}

/* vim :set ts=4 tw=4 tws=4 */
