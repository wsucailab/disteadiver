/**
 * File: src/distEA/distdistEA.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 02/05/15		hcai		created; for instrumenting distEA method events
 * 02/08/15		hcai		added instrumentation for intercepting network I/Os through socket input/output streams
 * 02/16/15		hcai		added instrumentation for intercepting network I/Os through java NIO socketChannel reads/writes
 * 02/18/15		hcai		handle both synchronous and asynchronous non-blocking network I/Os through java NIO channels
 * 02/27/15		hcai		to deal with distributed network I/Os using both socket and NIO, insert the dist_nioread/write probes
 * 							*before* the original i/o function calls to be consistent with the order of reading/writing clocks 
 * 							through socket i/o streams.
 * 07/30/15		hcai		added instrumentation for communication event monitoring for message passing via invocations to ObjectInputStream::read/writeObject
 * 4/20/16		hcai		eliminate the clumsy requirement for the dummy "___link()" static members in each class to be dynamically retrieved (for retrospective accesses)
*/
package distEA;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Iterator;
import java.util.Set;

import profile.InstrumManager;
import dua.Forensics;
import dua.global.ProgramFlowGraph;
import dua.method.CFG;
import dua.method.CFG.CFGNode;
import dua.util.Util;
import soot.*;
import soot.jimple.*;
import EAS.EAInst;
import MciaUtil.utils;

public class distEAMethodNum extends EAInst {
	
	/** distEA also monitors method return events besides those dealt with by EAInst */
	protected SootMethod mReturnFrom;
	
	protected SootMethod mNioRead;
	protected SootMethod mNioWrite;
	protected SootClass cDistSockInStream;
	protected SootClass cDistSockOutStream;
	
	protected SootMethod mObjStreamRead;
	protected SootMethod mObjStreamWrite;
	public static int methodNum=0;
	protected static distEAOptions opts = new distEAOptions();
	
	public static void main(String args[]){
		args = preProcessArgs(opts, args);

		distEAMethodNum disteaMethodNum = new distEAMethodNum();
		// examine catch blocks
		dua.Options.ignoreCatchBlocks = false;
		dua.Options.skipDUAAnalysis = true;
		
		if (opts.monitor_per_thread()) {
			Scene.v().addBasicClass("distEA.distThreadMonitor");
		}
		else {
			Scene.v().addBasicClass("distEA.distMonitor");
		}
		if (opts.use_socket()) {
			Scene.v().addBasicClass("distEA.distSocketInputStream");
			Scene.v().addBasicClass("distEA.distSocketOutputStream");
		}
		
		Forensics.registerExtension(disteaMethodNum);
		Forensics.main(args);
	}
	
	/**
	 * Descendants may want to use customized event monitors
	 */
	protected void init() {
//		if (opts.monitor_per_thread()) {
//			clsMonitor = Scene.v().getSootClass("distEA.distThreadMonitor");
//		}
//		else {
//			clsMonitor = Scene.v().getSootClass("distEA.distMonitor");
//		}
//		mInitialize = clsMonitor.getMethodByName("initialize");
//		mEnter = clsMonitor.getMethodByName("enter");
//		mReturnInto = clsMonitor.getMethodByName("returnInto");
//		mTerminate = clsMonitor.getMethodByName("terminate");
//		
//		mReturnFrom = clsMonitor.getMethodByName("returnFrom");
//		
//		cDistSockInStream = Scene.v().getSootClass("distEA.distSocketInputStream");
//		cDistSockOutStream = Scene.v().getSootClass("distEA.distSocketOutputStream");
//		mNioRead = clsMonitor.getMethodByName("dist_nioread");
//		mNioWrite = clsMonitor.getMethodByName("dist_niowrite");
//		
//		mObjStreamRead = clsMonitor.getMethodByName("dist_objstreamread");
//		mObjStreamWrite = clsMonitor.getMethodByName("dist_objstreamwrite");
	}
	
	public void run() {
		//System.out.println("Running distEA extension of DUA-Forensics");
		//StmtMapper.getCreateInverseMap();
		
		//super.instrument();
		
		this.instRetEvents();
		
	}
		
	/** monitoring return events, in addition to monitoring returned-into events, is needed in presence of multi-threaded executions */
	public void instRetEvents() {
		/* traverse all classes */
		Iterator<SootClass> clsIt = Scene.v().getClasses().iterator();
		while (clsIt.hasNext()) {
			SootClass sClass = (SootClass) clsIt.next();
			if ( sClass.isPhantom() ) {
				// skip phantom classes
				continue;
			}
			if ( !sClass.isApplicationClass() ) {
				// skip library classes
				continue;
			}
			
			/* traverse all methods of the class */
			Iterator<SootMethod> meIt = sClass.getMethods().iterator();
			while (meIt.hasNext()) {
				SootMethod sMethod = (SootMethod) meIt.next();
				if ( !sMethod.isConcrete() ) {
					// skip abstract methods and phantom methods, and native methods as well
					continue; 
				}
				if ( sMethod.toString().indexOf(": java.lang.Class class$") != -1 ) {
					// don't handle reflections now either
					continue;
				}
				// cannot instrument method event for a method without active body
				if ( !sMethod.hasActiveBody() ) {
					continue;
				}
				methodNum++;
				System.out.println(" methodNum="+methodNum);

			} // -- while (meIt.hasNext()) 
		} // -- while (clsIt.hasNext())
		System.out.println("Final methodNum="+methodNum);
	} // -- void instRetEvents



} // -- public class distEA  

/* vim :set ts=4 tw=4 tws=4 */

