package Diver;

import dua.Forensics;
import dua.Options;
import edu.ksu.cis.indus.staticanalyses.dependency.DependencyXMLizer;
import edu.ksu.cis.indus.staticanalyses.dependency.IDependencyAnalysis;
import edu.ksu.cis.indus.staticanalyses.dependency.InterferenceDAv1;
import edu.ksu.cis.indus.staticanalyses.dependency.InterferenceDAv2;
import edu.ksu.cis.indus.staticanalyses.dependency.InterferenceDAv3;
import edu.ksu.cis.indus.staticanalyses.dependency.ReadyDAv1;
import edu.ksu.cis.indus.staticanalyses.dependency.ReadyDAv2;
import edu.ksu.cis.indus.staticanalyses.dependency.ReadyDAv3;
import edu.ksu.cis.indus.staticanalyses.dependency.SynchronizationDA;
import edu.ksu.cis.indus.staticanalyses.interfaces.IValueAnalyzer;
import edu.ksu.cis.indus.staticanalyses.tokens.ITokens;
import fault.StmtMapper;

import soot.*;
import soot.util.dot.DotGraph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.cli.MissingArgumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import EAS.*;
import edu.ksu.cis.indus.staticanalyses.dependency.DependencyXMLizerCLI;

public class DiverInst2 extends EAInst {
	
	protected static DiverOptions opts = new DiverOptions();
	
	
	//Indus Options
	private static final Logger LOGGER = LoggerFactory.getLogger(DiverTest5.class);

	/**
	 * This is the flow analyser used by the analyses being tested.
	 */
	protected IValueAnalyzer<Value> aa;

	/**
	 * A collection of dependence analyses.
	 */
	protected List<IDependencyAnalysis> das = new ArrayList<IDependencyAnalysis>();

	/**
	 * This is a map from interface IDs to interface implementations that are required by the analyses being driven.
	 * 
	 * @invariant info.oclIsKindOf(Map(String, Object))
	 */
	protected final Map info = new HashMap();

	/**
	 * This indicates if common unchecked exceptions should be considered.
	 */
	private boolean commonUncheckedException;

	/**
	 * This flag indicates if jimple should be dumped.
	 */
	private boolean dumpJimple;

	/**
	 * This indicates if exceptional exits should be considered.
	 */
	private boolean exceptionalExits;

	/**
	 * This flag indicates if the simple version of aliased use-def information should be used.
	 */
	private boolean useAliasedUseDefv1;

	/**
	 * This indicates if safe lock should be used.
	 */
	private boolean useSafeLockAnalysis;

	/**
	 * The xmlizer used to xmlize dependence information.
	 */
	private final DependencyXMLizer xmlizer = new DependencyXMLizer();
	/**
	 * This provides object flow information required by this analysis.
	 */
	private static IValueAnalyzer<Value> analyzer;

	
	public static void main(String args[]){
		args = preProcessArgs(opts, args);

		DiverInst2 d2 = new DiverInst2();
		// examine catch blocks
		dua.Options.ignoreCatchBlocks = false;
		
		if (opts.onlineAll) {
			Scene.v().addBasicClass("Diver.EAMonitorAllInOne");
		}
        else if (opts.onlineOne) {
			Scene.v().addBasicClass("Diver.EAMonitorOneByOne");
		}
		else {
			Scene.v().addBasicClass("Diver.EAMonitor");
		}
		
		Forensics.registerExtension(d2);
		Forensics.main(args);
	}
	
	@Override protected void init() {
		if (opts.onlineAll) {
			clsMonitor = Scene.v().getSootClass("Diver.EAMonitorAllInOne");
		}
        else if (opts.onlineOne) {
			clsMonitor = Scene.v().getSootClass("Diver.EAMonitorOneByOne");
		}
		else {
			clsMonitor = Scene.v().getSootClass("Diver.EAMonitor");
		}
		clsMonitor.setApplicationClass();
		mInitialize = clsMonitor.getMethodByName("initialize");
		mEnter = clsMonitor.getMethodByName("enter");
		mReturnInto = clsMonitor.getMethodByName("returnInto");
		mTerminate = clsMonitor.getMethodByName("terminate");
	}
	
	@Override public void run() {
		System.out.println("Running Diver extension of DUA-Forensics");
		// we would want to retrieve the Jimple statement ids for the VTG nodes
		StmtMapper.getCreateInverseMap();
		
		// 1. create the static value transfer graph
		int ret = createVTGWithIndus();
		
		// 2. instrument EAS events
		instrument();
	}
	
	private int createVTGWithIndus() {
		StaticTransferGraph vtg = new StaticTransferGraph();
		try {
			final long startTime = System.currentTimeMillis();
			//if (0==vtg.buildGraph(opts.debugOut())) return 0;
			vtg.setIncludeIntraCD(opts.intraCD);
			vtg.setIncludeInterCD(opts.interCD);
			vtg.setExInterCD(opts.exceptionalInterCD);
			vtg.setIgnoreRTECD(opts.ignoreRTECD);
			vtg.buildGraph(opts.debugOut());
			System.out.println("vtg before Indus");
			System.out.println(vtg);
			
			StaticTransferGraph vtg2=AddEdgesWithIndus(vtg);
			System.out.println("vtg2 after Indus");
			System.out.println(vtg2);
			//vtg.AddThreadEdge(opts.debugOut(),"Dependence.log");
			final long stopTime = System.currentTimeMillis();
			System.out.println("VTG construction took " + (stopTime - startTime) + " ms");
	
//			vtg.addControlDependencies(opts.debugOut());
//			System.out.println("Computing control dependencies took " + (System.currentTimeMillis() - stopTime) + " ms");
			
			// DEBUG: validate the static VTG against static forward slice
			if (opts.validateVTG) {
				// as a part of the static VTG validation, automatically check if VTG misses any dependences involving object variables, including
				// library objects
				vtg2.checkObjvarDeps();
			}
		}
		catch (Exception e) {
			System.out.println("Error occurred during the construction of VTG");
			e.printStackTrace();
			return -1;
		}
		// DEBUG: test serialization and deserialization
		System.out.println("opts.serializeVTG=" + opts.serializeVTG);
		if (opts.serializeVTG) {
			System.out.println("opts.serializeVTG In");
			String fn = dua.util.Util.getCreateBaseOutPath() + "staticVtg.dat";
			if ( 0 == vtg.SerializeToFile(fn) ) {
				//if (opts.debugOut()) 
				{
					System.out.println("======== VTG successfully serialized to " + fn + " ==========");
					StaticTransferGraph g = new StaticTransferGraph();
					if (null != g.DeserializeFromFile (fn)) {
						System.out.println("======== VTG loaded from disk file ==========");
						//g.dumpGraphInternals(true);
						System.out.println(g);
					}
				}
			} // test serialization/deserialization
		} // test static VTG construction
		
		// DEBUG: visualize the VTG for paper writing purposes

		System.out.println("opts.visualizeVTG=" + opts.visualizeVTG);
		if (opts.visualizeVTG) {
			System.out.println("opts.visualizeVTG In");
			//String dotfn = soot.SourceLocator.v().getOutputDir() + java.io.File.separator + "staticVTG";
			String dotfn = dua.util.Util.getCreateBaseOutPath() + "staticVTG" + DotGraph.DOT_EXTENSION;
			vtg.visualizeVTG(dotfn);
		}
		return 0;
	} // -- createVTGWithIndus
	
	private StaticTransferGraph AddEdgesWithIndus(StaticTransferGraph svtg) {
		Object[][] _dasOptions = {//				
				{"sda", "Synchronization dependence", new SynchronizationDA(svtg)},
				{"frda1", "Forward Ready dependence v1", ReadyDAv1.getForwardReadyDA(svtg)},
//				{"brda1", "Backward Ready dependence v1", ReadyDAv1.getBackwardReadyDA(svtg)},
//				{"frda2", "Forward Ready dependence v2", ReadyDAv2.getForwardReadyDA(svtg)},
//				{"brda2", "Backward Ready dependence v2", ReadyDAv2.getBackwardReadyDA(svtg)},
//				{"frda3", "Forward Ready dependence v3", ReadyDAv3.getForwardReadyDA(svtg)},
//				{"brda3", "Backward Ready dependence v3", ReadyDAv3.getBackwardReadyDA(svtg)},
				{"ida1", "Interference dependence v1", new InterferenceDAv1(svtg)},
//				{"ida2", "Interference dependence v2", new InterferenceDAv2(svtg)},
//				{"ida3", "Interference dependence v3", new InterferenceDAv3(svtg)},
				};
		DependencyXMLizerCLI _xmlizerCLI = new DependencyXMLizerCLI();
		_xmlizerCLI.xmlizer.setXmlOutputDir("/tmp");
		_xmlizerCLI.dumpJimple = false;
		_xmlizerCLI.useAliasedUseDefv1 = false;
		_xmlizerCLI.useSafeLockAnalysis = false;
		_xmlizerCLI.exceptionalExits = false;
		_xmlizerCLI.commonUncheckedException = false;
		final List<String> _classNames = new ArrayList<String>();
		for (SootClass sClass:Scene.v().getApplicationClasses()) 
		{
			_classNames.add(sClass.toString());
		}	
		if (_classNames.isEmpty()) {
			System.out.println("Please specify at least one class.");
		}
		System.out.println("_classNames="+_classNames);
		_xmlizerCLI.setClassNames(_classNames);
		if (!_xmlizerCLI.parseForDependenceOptions(_dasOptions,_xmlizerCLI)) {
			System.out.println("At least one dependence analysis must be requested.");
		}
		System.out.println("_xmlizerCLI.das.size(): " + _xmlizerCLI.das.size());
		
		_xmlizerCLI.<ITokens> execute();
		System.out.println("svtg");
		System.out.println(svtg);
		return svtg;
	}
} // -- public class DiverInst2  

/* vim :set ts=4 tw=4 tws=4 */