/**
 * File: src/Diver/DiverRunAllInOne.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 12/31/15		hcai		created; for runtime all-in-one impact computation
 * 01/01/16		hcai		reached the working version 
 * 01/05/16     hcai        added an option to run while dealing with impact computation for specified queries
*/
package Diver;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
		
public class DiverRunAllInOne {
	public static void main(String args[]){
		if (args.length < 3) {
			System.err.println("Too few arguments: \n\t " +
					"DiverRunAllInOne subjectName subjectDir binPath \n\n");
			return;
		}
		String subjectName = args[0];

		String subjectDir = args[1]; 
		String binPath = args[2];
		
		Set<String> queries = null;
		
		if (args.length > 3) {
			File queryF = new File(args[3]);
			if (queryF.exists()) {
				queries = new HashSet<String>();
				readQueries(queries, queryF.getAbsolutePath());
			}
			else {
				System.err.println("invalid query list.");
				return;
			}
		}
		
		System.out.print("Subject: " + subjectName + " Dir=" + subjectDir + " binpath=" + binPath );
		if (queries != null) {
			System.out.println (queries.size() + " queries");
		}
		else System.out.println();
		
		ImpactAllInOne.setSVTG(binPath+File.separator+"staticVtg.dat");
		if (0 != ImpactAllInOne.initializeGraph(false)) {
			System.out.println("Unable to load the static value transfer graph, aborted now.");
			return;
		}
		
		try {
			if (queries!=null) {
				int qcnt = 0;
				for (String query : queries) {
					System.out.println("===== on query " + (++qcnt) + "/" + queries.size() + " =====");
					ImpactAllInOne.setQuery(query);
					startRunSubject(subjectName, subjectDir, binPath);
				}
			}
			else {
				startRunSubject(subjectName, subjectDir, binPath);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void readQueries(Set<String> queryMethods, String fnQuerylist) {
		BufferedReader br;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(fnQuerylist)));
			String ts = br.readLine();
			while(ts != null) {
				queryMethods.add(ts.trim());
				ts = br.readLine();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void startRunSubject(final String name, String dir, final String binPath){
		int n = 0;
		BufferedReader br;
		
		/** a parallel version 
		final List<String[]> allinputargs = new ArrayList<String[]>();
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(dir+"/inputs/testinputs.txt")));
			String ts = br.readLine();
			while(ts != null){
				String [] args = preProcessArg(ts,dir);
				allinputargs.add(args);
				ts = br.readLine();
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		ExecutorService es = Executors.newCachedThreadPool();
		for(final int i=0;i<allinputargs.size();i++) {
		    es.execute(new Runnable() {
				@Override public void run() {
					File runSub = new File(binPath);
					URL url = runSub.toURL();
				    URL[] urls = new URL[]{url};
				    
				    ImpactAllInOne icAgent = new ImpactAllInOne();
				    EAMonitorAllInOne.setICAgent(icAgent);
				   
				    try {
				    	
				    	ClassLoader cl = new URLClassLoader( urls, ClassLoader.getSystemClassLoader() );
					    Class cls = cl.loadClass(name);
					    
					    Method me=cls.getMethod("main", new Class[]{allinputargs.get(i).getClass()});
					    me.invoke(null, new Object[]{(Object)allinputargs.get(i)});
					}
				    catch (InvocationTargetException e) {
				    	e.getTargetException().printStackTrace();
				    }
					catch (Exception e) {
						e.printStackTrace();
					}

				   // invoke the "program termination event" for the subject in case there is uncaught exception occurred
				   EAMonitorAllInOne.terminate("Enforced by DiverRun.");
				}
			});
		}
		es.shutdown();
		boolean finshed = es.awaitTermination(1, TimeUnit.MINUTES);
		*/
		
		ImpactAllInOne icAgent = new ImpactAllInOne();
		EAMonitorAllInOne.setICAgent(icAgent);
		   
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(dir+"/inputs/testinputs.txt")));
			String ts = br.readLine();
			while(ts != null){
				n++;
				System.out.println("current at the test No.  " + n);
				String [] args = preProcessArg(ts,dir);
				
				File runSub = new File(binPath);
				URL url = runSub.toURL();
			    URL[] urls = new URL[]{url};
			    
			    icAgent.resetImpOPs();
			    EAMonitorAllInOne.resetInternals();
			   
			    try {
			    	/*
					ClassLoader parentloader = new URLClassLoader(urls2);
				    ClassLoader cl = new URLClassLoader( urls, Thread.currentThread().getContextClassLoader() );				    
				    Thread.currentThread().setContextClassLoader(cl);
				    */
			    	ClassLoader cl = new URLClassLoader( urls, ClassLoader.getSystemClassLoader() );
				    Class cls = cl.loadClass(name);
				    
				    Method me=cls.getMethod("main", new Class[]{args.getClass()});
				    me.invoke(null, new Object[]{(Object)args});
				}
			    catch (InvocationTargetException e) {
			    	e.getTargetException().printStackTrace();
			    }
				catch (Exception e) {
					e.printStackTrace();
				}

			   // invoke the "program termination event" for the subject in case there is uncaught exception occurred
			   EAMonitorAllInOne.terminate("Enforced by DiverRunAllInOne.");

			   ts = br.readLine();
			}
			
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		icAgent.dumpAllImpactSets();
	}

	public static String[] preProcessArg(String arg,String dir){
		String s1 = arg.replaceAll("\\\\+","/").replaceAll("\\s+", " ");
 
		if(s1.startsWith(" "))
			s1 = s1.substring(1,s1.length());
		String argArray[] =  s1.split(" ");
		for(int i=0;i<argArray.length;i++){
			if(argArray[i].startsWith("..")){
				argArray[i] = argArray[i].replaceFirst("..", dir);
			}
		}		
		return argArray;
	}
}

/* vim :set ts=4 tw=4 tws=4 */
