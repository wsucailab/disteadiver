package Diver;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

//import edu.ksu.cis.indus.common.collections.IteratorUtils;
//import edu.ksu.cis.indus.common.collections.MapUtils;
//import edu.ksu.cis.indus.common.soot.Constants;
//import edu.ksu.cis.indus.common.soot.SootPredicatesAndTransformers;
//import edu.ksu.cis.indus.common.soot.Util;
import soot.jimple.spark.SparkTransformer;
import soot.options.Options;
import soot.*;
import soot.JastAddJ.ReturnStmt;
import soot.JastAddJ.ThrowStmt;
import soot.jimple.*;
//import edu.ksu.cis.indus.annotations.NonNull;
//import edu.ksu.cis.indus.annotations.NonNullContainer;
//import edu.ksu.cis.indus.common.collections.IteratorUtils;
//import edu.ksu.cis.indus.common.datastructures.HistoryAwareFIFOWorkBag;
//import edu.ksu.cis.indus.common.datastructures.IWorkBag;
//import edu.ksu.cis.indus.common.datastructures.Triple;
//import edu.ksu.cis.indus.common.soot.SootPredicatesAndTransformers;
//import edu.ksu.cis.indus.interfaces.ICallGraphInfo;
//import edu.ksu.cis.indus.interfaces.IEnvironment;
//import edu.ksu.cis.indus.interfaces.ICallGraphInfo.CallTriple;
//import edu.ksu.cis.indus.processing.Context;
//import edu.ksu.cis.indus.staticanalyses.interfaces.IValueAnalyzer;
public class DiverTest2 {
	public static String path = "C:/Research/multichat/bin"; //"C:/Research/nioecho/bin";  ///voldemort/rest/coordinator/admin  C:/Soot/In
	static boolean debugOut = true;
//	private final Collection<SootMethod> threadEntryPoints = new HashSet<SootMethod>();
//	private final Map<Triple<InvokeStmt, SootMethod, SootClass>, Collection<SootMethod>> thread2methods = new HashMap<Triple<InvokeStmt, SootMethod, SootClass>, Collection<SootMethod>>();
//	private final Map<SootMethod, Collection<Triple<InvokeStmt, SootMethod, SootClass>>> method2threadCreationSite = new HashMap<SootMethod, Collection<Triple<InvokeStmt, SootMethod, SootClass>>>(
//			Constants.getNumOfMethodsInApplication());
	/**
	 * This provides object flow information required by this analysis.
	 */
//	private static IValueAnalyzer<Value> analyzer;

	/**
	 * This provides call graph information pertaining to the system.
	 * 
	 * @invariant cgi != null
	 */
//	private final ICallGraphInfo cgi;
	public static void main(String args[]) {
		
//      if(args.length == 0)
//      {
//          System.out.println("Usage: dtSourceSink directory");
//          System.exit(0);
//      }            
//      else
//			System.out.println("[mainClass]"+args[0]);	
//      path = args[0]; 
		
	
		initial(path);
		//enableSpark(path);
		//SootClass _sc=Scene.v().loadClassAndSupport("C:\\Soot\\In\\Acount.class");
		//System.out.println("_sc="+_sc);
		//_sc.setApplicationClass();
		SootClass THREAD_CLASS = Scene.v().getSootClass("java.lang.Thread"); 
		FastHierarchy har = new FastHierarchy();
		for (SootClass sClass:Scene.v().getApplicationClasses()) 
		{
			
			if (har.isSubclass(sClass, THREAD_CLASS))
			{
					System.out.println(" THREAD_CLASS : "+sClass);
			}
			else if (sClass.implementsInterface("java.lang.Runnable"))
			{
				System.out.println(" RUNNABLE_CLASS : "+sClass);
			}
			System.out.println("**************************sClass: "+sClass);
////			final SootMethod _runMethod = sClass.getMethodByName("run"); 
////			if (_runMethod!=null)
////				System.out.println(" _startMethod="+_runMethod);
//			if ( sClass.isPhantom() ) {	continue; }
//			if ( !sClass.isConcrete() ) {	continue; }
			
			for(SootMethod m:sClass.getMethods())
			{
				System.out.println("---------- method= "+m);
	            if (m.isSynchronized()) {
	            	System.out.println("	---------- method.isSynchronized()= "+m);
	            }
				if ( !m.isConcrete() ) {
					// skip abstract methods and phantom methods, and native methods as well
					System.out.println("	---------- !m.isConcrete()= "+m);
					//continue;
					//continue; 
				}
//				if ( m.toString().indexOf(": java.lang.Class class$") != -1 ) {
//					// don't handle reflections now either
//					continue;
//				}
//				
				// cannot instrument method event for a method without active body
				if ( !m.hasActiveBody() ) {
					System.out.println("	---------- !m.hasActiveBody= "+m);
					//continue;
				}
//				
//				//System.out.println("m.retrieveActiveBody()="+m.retrieveActiveBody());
//				Iterator<Unit> units=m.retrieveActiveBody().getUnits().iterator();
//				//System.out.println("units="+units);
//				while(units.hasNext())
//				{
//					Unit u=units.next();
//					System.out.println(m+" - "+ u.toString());
//					/*
//					Stmt st=(Stmt) u;	
//					if (st instanceof EnterMonitorStmt)
//					{
//						System.out.println("EnterMonitorStmt st= "+m+" - "+st);
//					}
//					else if (st instanceof ExitMonitorStmt)
//					{
//						System.out.println("ExitMonitorStmt st= "+m+" - "+st);
//					}
//					else if (st instanceof GotoStmt)
//					{
//						System.out.println(" GotoStmt st= "+m+" - "+st);
//					}
//					else if(st instanceof AssignStmt) 
//					{
//						System.out.println(" AssignStmt st= "+m+" - "+st);
//					}
//					else if (st instanceof DefinitionStmt)
//					{
//						System.out.println(" DefinitionStmt st= "+m+" - "+st);
//					}
//					else if (st instanceof IdentityStmt)
//					{
//						System.out.println(" IdentityStmt st= "+m+" - "+st);
//					}
//					else if (st instanceof IfStmt)
//					{
//						System.out.println(" IfStmt st= "+m+" - "+st);
//					}
//					else if (st instanceof InvokeStmt)
//					{
//						System.out.println(" InvokeStmt st= "+m+" - "+st);
//						InvokeStmt ist=(InvokeStmt) st;
//						System.out.println(" InvokeStmt ist= "+ist);
//
//					}
//					else if (st instanceof LookupSwitchStmt)
//					{
//						System.out.println(" LookupSwitchStmt st= "+m+" - "+st);
//					}
//					else if (st instanceof MonitorStmt)
//					{
//						System.out.println(" MonitorStmt st= "+m+" - "+st);
//					}			
//					else if (st instanceof NopStmt)
//					{
//						System.out.println(" NopStmt st= "+m+" - "+st);
//					}
//					else if (st instanceof RetStmt)
//					{
//						System.out.println(" RetStmt st= "+m+" - "+st);
//					}
//					else if (st instanceof ReturnStmt)
//					{
//						System.out.println(" ReturnStmt st= "+m+" - "+st);
//					}
//					else if (st instanceof ReturnVoidStmt)
//					{
//						System.out.println(" ReturnVoidStmt st= "+m+" - "+st);
//					}
//					else if (st instanceof TableSwitchStmt)
//					{
//						System.out.println(" TableSwitchStmt st= "+m+" - "+st);
//					}
//					else if (st instanceof ThrowStmt)
//					{
//						System.out.println(" ThrowStmt st= "+m+" - "+st);
//					}		
//					else if (st instanceof MonitorStmt)
//					{
//						System.out.println(" MonitorStmt st= "+m+" - "+st);
//					}						
//					else
//						System.out.println("Other type st= "+m+" - "+st);
//					*/
//				}	
				
//				Body b=m.retrieveActiveBody();
//				System.out.println("[body]="+b);
				/*
				Iterator<Unit> units=m.retrieveActiveBody().getUnits().snapshotIterator();
				while(units.hasNext())
				{
					Unit u=units.next();
					System.out.println("u="+u);
				
				}
//				*/
			}	
			
		}
//		final IEnvironment _env = analyzer.getEnvironment();
//		final SootClass _threadClass = _env.getClass("java.lang.Thread");
//		final SootMethod _startMethod = _threadClass.getMethodByName("start"); 
	}
 
	// soot option 1
	private static void initial(String classPath) {
		soot.G.reset();
		Options.v().set_allow_phantom_refs(true);
		Options.v().set_process_dir(Collections.singletonList(classPath));//
		Options.v().set_whole_program(true);
		Scene.v().loadNecessaryClasses();
		
	}
	
	// soot option 2
    private static void enableSpark(String path){
        HashMap opt = new HashMap();
        //opt.put("verbose","true");
        //opt.put("propagator","worklist");
        opt.put("simple-edges-bidirectional","false");
        //opt.put("on-fly-cg","true");
        opt.put("apponly", "true");
//        opt.put("set-impl","double");
//        opt.put("double-set-old","hybrid");
//        opt.put("double-set-new","hybrid");
//        opt.put("allow-phantom-refs", "true");
        opt.put("-process-dir",path);
        
        SparkTransformer.v().transform("",opt);
    }


	private static SootMethod getRunMethod(final SootClass threadClass) {
		return threadClass.getMethod("run", Collections.EMPTY_LIST, VoidType.v());
	}
	
//	private static Collection<Value> getRunnables(final SootClass threadClass) {
//	Collection<Value> _result = Collections.emptySet();
//	final SootMethod _threadRunMethod = getRunMethod(threadClass);
//
//	final Iterator<Unit> _units = _threadRunMethod.retrieveActiveBody().getUnits().iterator();
//	for (final Iterator<Unit> _iter = IteratorUtils.filteredIterator(_units,
//			SootPredicatesAndTransformers.INVOKING_UNIT_PREDICATE); _iter.hasNext();) {
//		final Unit _stmt = _iter.next();
//		final InvokeExpr _expr = ((InvokeStmt) _stmt).getInvokeExpr();
//		final SootMethod _invokedMethod = _expr.getMethod();
//
//		if (_expr instanceof InterfaceInvokeExpr && _invokedMethod.getName().equals("run")
//				&& _invokedMethod.getDeclaringClass().getName().equals("java.lang.Runnable")) {
//			final InterfaceInvokeExpr _iExpr = (InterfaceInvokeExpr) _expr;
//			final Context _context = new Context();
//			_context.setRootMethod(_threadRunMethod);
//			_context.setStmt((Stmt) _stmt);
//			_context.setProgramPoint(_iExpr.getBaseBox());
//			_result.add((Value) _threadRunMethod);   //analyzer.getValues(_iExpr.getBase(), _context);
//			break;
//		}
//	}
//	return _result;
//}

/**
 * Retrieves the runnable methods in the given class
 * 
 * @param clazz of interest.
 * @return a collection of runnable methods in the given class.
 */
//	@NonNull @NonNullContainer
//	static Collection<SootMethod> getRunnableMethods(@NonNull final SootClass clazz) {
//		final Collection<Value> _runnables = getRunnables(clazz);
//		final Collection<SootMethod> _result = new HashSet<SootMethod>(_runnables.size());
//		for (final Iterator<Value> _j = IteratorUtils.filteredIterator(_runnables.iterator(),
//				SootPredicatesAndTransformers.NEW_EXPR_PREDICATE); _j.hasNext();) {
//			final NewExpr _temp = (NewExpr) _j.next();
//			final SootClass _runnable = clazz;
//			final SootMethod _entryPoint = getRunMethod(_runnable);
//			_result.add(_entryPoint);
//		}
//		
//		return _result;
//	}
	
//	private void calculateThreadCallGraph(boolean debugOut) throws RuntimeException {
//		// capture the run call-site in Thread.start method
//		final IEnvironment _env = analyzer.getEnvironment();
//		final SootClass _threadClass = _env.getClass("java.lang.Thread");
//		final SootMethod _startMethod = _threadClass.getMethodByName("start");
//		final Context _ctxt = new Context();
//		_ctxt.setRootMethod(_startMethod);
//
//		final Collection<Value> _values = analyzer.getValuesForThis(_ctxt);
//		final Map<SootClass, Collection<SootMethod>> _class2runCallees = new HashMap<SootClass, Collection<SootMethod>>();
//		final Collection<SootMethod> _runnables = getRunnableMethods(_threadClass);
//
//		if (debugOut) {
//			System.out.println("Thread expressions are: " + _values);
//			System.out.println("Runnables are: " + _runnables);
//		}
//
//		for (final Iterator<Value> _i = IteratorUtils.filteredIterator(_values.iterator(),
//				SootPredicatesAndTransformers.NEW_EXPR_PREDICATE); _i.hasNext();) {
//			final NewExpr _value = (NewExpr) _i.next();
//			final SootClass _sc = _env.getClass(_value.getBaseType().getClassName());
//			Collection<SootMethod> _methods;
//
//			if (!_class2runCallees.containsKey(_sc)) {
//				boolean _flag = false;
//
//				final SootClass _scTemp = Util.getDeclaringClass(_sc, "run", Collections.<Type> emptyList(),
//						VoidType.v());
//
//				if (_scTemp != null) {
//					_flag = _scTemp.getName().equals("java.lang.Thread");
//				} else {
//					System.out.println("How can there be a descendent of java.lang.Thread without access to run() method.");
//					throw new RuntimeException("run() method is unavailable via " + _sc
//							+ " even though it is a descendent of java.lang.Thread.");
//				}
//
//				if (_flag) {
//					_methods = new HashSet<SootMethod>();
//
//					for (final Iterator<SootMethod> _j = _runnables.iterator(); _j.hasNext();) {
//						final SootMethod _entryPoint = _j.next();
//						threadEntryPoints.add(_entryPoint);
//						_methods.addAll(transitiveThreadCallClosure(_entryPoint));
//					}
//				} else {
//					final SootMethod _entryPoint = getRunMethod(_sc);
//					threadEntryPoints.add(_entryPoint);
//					_methods = transitiveThreadCallClosure(_entryPoint);
//				}
//
//				_class2runCallees.put(_sc, _methods);
//			}
//		}
//
//		final Collection<CallTriple> _callTriples = cgi.getCallers(_startMethod);
//		final Iterator<CallTriple> _i = _callTriples.iterator();
//		final int _iEnd = _callTriples.size();
//
//		for (int _iIndex = 0; _iIndex < _iEnd; _iIndex++) {
//			final CallTriple _ctrp = _i.next();
//			final SootMethod _caller = _ctrp.getMethod();
//			_ctxt.setRootMethod(_caller);
//
//			final InvokeStmt _callStmt = (InvokeStmt) _ctrp.getStmt();
//			_ctxt.setStmt(_callStmt);
//
//			final VirtualInvokeExpr _virtualInvokeExpr = (VirtualInvokeExpr) _callStmt.getInvokeExpr();
//			_ctxt.setProgramPoint(_virtualInvokeExpr.getBaseBox());
//
//			final Collection<Value> _baseValues = analyzer.getValues(_virtualInvokeExpr.getBase(), _ctxt);
//
//			for (final Iterator<Value> _j = IteratorUtils.filteredIterator(_baseValues.iterator(),
//					SootPredicatesAndTransformers.NEW_EXPR_PREDICATE); _j.hasNext();) {
//				final NewExpr _value = (NewExpr) _j.next();
//				final SootClass _sc = _env.getClass(_value.getBaseType().getClassName());
//				if (_class2runCallees.containsKey(_sc)) {
//					final Collection<SootMethod> _methods = _class2runCallees.get(_sc);
//					final Triple<InvokeStmt, SootMethod, SootClass> _thread = new Triple<InvokeStmt, SootMethod, SootClass>(
//							_callStmt, _caller, _sc);
//					MapUtils.putAllIntoCollectionInMap(thread2methods, _thread, _methods);
//
//					for (final Iterator<SootMethod> _k = _methods.iterator(); _k.hasNext();) {
//						final SootMethod _sm = _k.next();
//						MapUtils.putIntoCollectionInMap(method2threadCreationSite, _sm, _thread);
//					}
//				}
//			}
//		}
//	}
//	private Collection<SootMethod> transitiveThreadCallClosure(final SootMethod starterMethod) {
//		final Collection<SootMethod> _result = new HashSet<SootMethod>();
//		final IWorkBag<SootMethod> _wb = new HistoryAwareFIFOWorkBag<SootMethod>(_result);
//		_wb.addWork(starterMethod);
//		_result.add(starterMethod);
//
//		while (_wb.hasWork()) {
//			final SootMethod _sm = _wb.getWork();
//
//			if (!Util.isStartMethod(_sm)) {
//				final Collection<CallTriple> _callees = cgi.getCallees(_sm);
//
//				for (final Iterator<CallTriple> _i = _callees.iterator(); _i.hasNext();) {
//					final CallTriple _ctrp = _i.next();
//					final SootMethod _temp = _ctrp.getMethod();
//
//					_wb.addWorkNoDuplicates(_temp);
//				}
//			}
//		}
//		return _result;
//	}
}