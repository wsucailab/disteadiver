/**
 * File: src/Diver/DiverAllInOneAll.java
*/
package Diver;
import java.io.*;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DiverAllInOneAll{
	static Set<String> changeSet = new LinkedHashSet<String>();
	static Map<String, Set<String> > impactSets = new LinkedHashMap<String, Set<String>>();
	static int nExecutions = Integer.MAX_VALUE;

	/* the All-in-One impact computation agent */
	//static final ImpactAllInOne icAgent = new ImpactAllInOne();
	
	static boolean debugOut = false;
	
	public static void main(String args[]){
		if (args.length < 3) {
			System.err.println("Too few arguments: \n\t " +
					"DiverAllInOneAll changedMethods traceDir binDir [numberTraces] [debugFlag]\n\n");
			return;
		}
		
		String queryFile = args[0]; // tell the changed methods, separated by comma if there are more than one
		String traceDir = args[1]; // tell the directory where execution traces can be accessed
		String binDir = args[2]; // tell the directory where the static value transfer graph binary can be found
		
		// read at most N execution traces if specified, otherwise exhaust all to be found
		if (args.length > 3) {
			nExecutions = Integer.parseInt(args[3]);
		}
		
		if (args.length > 4) {
			debugOut = args[4].equalsIgnoreCase("-debug");
		}
		if (debugOut) {
			System.out.println("Try to handle query file: " + queryFile);
		}
//		if (debugOut) {
//			System.out.println("Try to read [" + (-1==nExecutions?"All available":nExecutions) + "] traces in " 
//					+ traceDir + " with changed methods being " + changedMethods);
//		}
		
		// initialize the dynamic VTG with the static counterpart
		if (init(binDir) != 0) {
			// something wrong during the initialization of the dynamic graph
			return;
		}
		
		FileReader reader = null;  
        BufferedReader br = null; 
        try 
        {  
            reader = new FileReader(queryFile);     
             String str = "";    
             String changedMethods = "";      
             br = new BufferedReader(reader);  
    
            
             while ((str = br.readLine()) != null) {  
             	if (str.startsWith("<") && (str.indexOf(">")>2))
             	{
             		changedMethods=str.trim();       
             		if (debugOut) 
            			System.out.println("Try to read [" + (-1==nExecutions?"All available":nExecutions) + "] traces in " 
    					+ traceDir + " with changed methods being " + changedMethods);
             		
             		long time0 = System.currentTimeMillis();
             		// Parse
             		startParseTracesParallel(changedMethods, traceDir);
    				// Restore dvtg to old initial version
    				//dvtg.deepCopyFrom(dvtgBackup);
    				long time1 = System.currentTimeMillis();
    				System.out.println("RunTime for  elapsed:  " + (time1 - time0) + "milliseconds");
    				
             	}  //if
             }  //while
    
             br.close();  
             reader.close();              
          }   
          catch (Exception e) {  
             e.printStackTrace();  
         } 
        
		
		
	}
	
	public static int init(String binDir) {
		ImpactAllInOne.setSVTG(binDir+File.separator+"staticVtg.dat");
		if (0 != ImpactAllInOne.initializeGraph(debugOut)) {
			System.out.println("Unable to load the static value transfer graph, aborted now.");
			return -1;
		}
		return 0;
	}
	
	/** exercise the static graph and query impacts for a single execution trace */
	public static int parseSingleTrace(ImpactAllInOne icAgent, String traceDir, int tId, List<String> validChgSet, 
			Map<String, Set<String>> localImpactSets) throws Exception {
		try {
			String fnSource = traceDir  + File.separator + "test" + tId + ".em";
			if (debugOut) {
				System.out.println("\nProcessing execution trace in " + fnSource);
			}
			
			// 1. compute the dynamic VTG using the current execution trace
			icAgent.setTrace(fnSource);
			
			// 2. compute the local impact set : the impact set with respect to the current execution trace
			for (String chg : validChgSet) {
				Set<String> is = localImpactSets.get(chg);
				if (null == is) {
					is = new LinkedHashSet<String>();
					localImpactSets.put(chg, is);
				}
				
				is.addAll(icAgent.getImpactSet(chg));
			}
		}
		catch (Exception e) {
			throw e;
		}
		return localImpactSets.size();
	}
	
	public static int obtainValidChangeSet(String changedMethods) {
		changeSet.clear();  // in case this method (startParseTraces) gets multiple invocations from external callers 
		List<String> Chglist = dua.util.Util.parseStringList(changedMethods, ';');
		if (Chglist.size() < 1) {
			// nothing to do
			System.err.println("Empty query, nothing to do.");
			return -1;
		}
		// determine the valid change set
		Set<String> validChgSet = new LinkedHashSet<String>();
		for (String chg : Chglist) {
			validChgSet.addAll(ImpactAllInOne.getChangeSet(chg));
		}
		if (validChgSet.isEmpty()) {
			// nothing to do
			// System.out.println("Invalid queries, nothing to do.");
			return 0;
		}
		changeSet.addAll(validChgSet);
		return changeSet.size();
	}
	public static Set<String> getChangeSet() {
		return changeSet;
	}
	
	public static void startParseTraces(String changedMethods, String traceDir) {
		int tId;
		impactSets.clear(); // in case this method (startParseTraces) gets multiple invocations from external callers
		
		int nret = obtainValidChangeSet(changedMethods);
		if ( nret <= 0 ) {
			// nothing to do
			if (nret == 0) {
				// always output report so that post-processing script can work with the Diver result in a consistent way as if there were
				// some non-empty results
				printStatistics(impactSets, true);
			}
			return;
		}
		
		ImpactAllInOne.setQueries(changeSet);
		
		for (tId = 1; tId <= nExecutions; ++tId) {
			// impact sets relative to the current execution trace
			Map<String, Set<String>> localImpactSets = new LinkedHashMap<String, Set<String>>();
			
			try {
				
				if ( parseSingleTrace(new ImpactAllInOne(), traceDir, tId, new LinkedList<String>(changeSet), localImpactSets) < 0 ) {
					// ignore erroneous or problematic traces
					continue;
				}
				
				// -- DEBUG
				if (debugOut) {
					if (!localImpactSets.isEmpty()) {
						System.out.println("Impact set computed from current trace [no. " + tId + "]:");
						printStatistics(localImpactSets, false);
					}
				}
				
				// merge impact set across all execution traces
				for (String chg : localImpactSets.keySet()) {
					if (impactSets.get(chg) == null) {
						impactSets.put(chg, new LinkedHashSet<String>());
					}
					impactSets.get(chg).addAll(localImpactSets.get(chg));
				}
			}
			catch (FileNotFoundException e) { 
				break;
			}
			catch (IOException e) {
				throw new RuntimeException(e); 
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		System.out.println(--tId + " execution traces have been processed.");
		printStatistics(impactSets, true);
	}
	
	private static void printStatistics (Map<String, Set<String>> mis, boolean btitle) {
		if (btitle) {
			System.out.println("\n============ Diver Result ================");
			System.out.println("[Valid Change Set]");
			for (String m:changeSet) {
				System.out.println(m);
			}
		}
		Set<String> aggregatedIS = new LinkedHashSet<String>();
		for (String m : mis.keySet()) {
			System.out.println("[Change Impact Set of " + m + "]: size= " + mis.get(m).size());
			for (String im : mis.get(m)) {
				System.out.println(im);
			}
			// merge impact sets of all change queries
			aggregatedIS.addAll(mis.get(m));
		}
		if (btitle) {
			System.out.println("\n[Change Impact Set of All Changes]: size= " + aggregatedIS.size());
			for (String im : aggregatedIS) {
				System.out.println(im);
			}
		}
	}
	
	
	
	public static void startParseTracesParallel(String changedMethods, final String traceDir) {
		int tId;
		impactSets.clear(); // in case this method (startParseTraces) gets multiple invocations from external callers
		
		int nret = obtainValidChangeSet(changedMethods);
		if ( nret <= 0 ) {
			// nothing to do
			if (nret == 0) {
				// always output report so that post-processing script can work with the Diver result in a consistent way as if there were
				// some non-empty results
				printStatistics(impactSets, true);
			}
			return;
		}
		
		ImpactAllInOne.setQueries(changeSet);
		
		final class traceWorker extends Thread {
			public Map<String, Set<String>> localImpactSets = new LinkedHashMap<String, Set<String>>();
			private int tid;
			void setTid(int _tid) {this.tid = _tid;}
			@Override
			public void run() {
				try {
					parseSingleTrace(new ImpactAllInOne(), traceDir, tid, new LinkedList<String>(changeSet), localImpactSets);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		traceWorker[] allWorkers = new traceWorker[nExecutions];
		for (tId = 0; tId < nExecutions; ++tId) {
			allWorkers[tId] = new traceWorker();
			allWorkers[tId].setTid(tId+1);
			allWorkers[tId].start();
		}
		for (tId = 0; tId < nExecutions; ++tId) {
			try {
				allWorkers[tId].join();				
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		for (tId = 0; tId < nExecutions; ++tId) {
			// impact sets relative to the current execution trace
			// merge impact set across all execution traces
			for (String chg : allWorkers[tId].localImpactSets.keySet()) {
				if (impactSets.get(chg) == null) {
					impactSets.put(chg, new LinkedHashSet<String>());
				}
				impactSets.get(chg).addAll(	allWorkers[tId].localImpactSets.get(chg));
			}
		}
		
		System.out.println(tId + " execution traces have been processed.");
		printStatistics(impactSets, true);
	}
}

/* vim :set ts=4 tw=4 tws=4 */
