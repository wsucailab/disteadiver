package Diver;
import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.cli.CommandLine;

import edu.ksu.cis.indus.common.datastructures.Pair.PairManager;
import edu.ksu.cis.indus.common.soot.NamedTag;
import edu.ksu.cis.indus.common.soot.SootBasedDriver;
import edu.ksu.cis.indus.interfaces.ICallGraphInfo;
import edu.ksu.cis.indus.interfaces.IEnvironment;
import edu.ksu.cis.indus.interfaces.IEscapeInfo;
import edu.ksu.cis.indus.interfaces.IReadWriteInfo;
import edu.ksu.cis.indus.interfaces.IThreadGraphInfo;
import edu.ksu.cis.indus.processing.IProcessor;
import edu.ksu.cis.indus.processing.OneAllStmtSequenceRetriever;
import edu.ksu.cis.indus.processing.TagBasedProcessingFilter;
import edu.ksu.cis.indus.staticanalyses.callgraphs.CallGraphInfo;
import edu.ksu.cis.indus.staticanalyses.callgraphs.OFABasedCallInfoCollector;
import edu.ksu.cis.indus.staticanalyses.cfg.CFGAnalysis;
import edu.ksu.cis.indus.staticanalyses.concurrency.MonitorAnalysis;
import edu.ksu.cis.indus.staticanalyses.concurrency.escape.EquivalenceClassBasedEscapeAnalysis;
import edu.ksu.cis.indus.staticanalyses.concurrency.independence.IndependentRegionDetector;
import edu.ksu.cis.indus.staticanalyses.concurrency.independence.IndependentStmtDetector;
import edu.ksu.cis.indus.staticanalyses.concurrency.independence.IndependentStmtDetectorv2;
import edu.ksu.cis.indus.staticanalyses.flow.instances.ofa.OFAnalyzer;
import edu.ksu.cis.indus.staticanalyses.flow.processors.ThreadGraph;
import edu.ksu.cis.indus.staticanalyses.interfaces.IValueAnalyzer;
import edu.ksu.cis.indus.staticanalyses.processing.AnalysesController;
import edu.ksu.cis.indus.staticanalyses.processing.CGBasedProcessingFilter;
import edu.ksu.cis.indus.staticanalyses.processing.ValueAnalyzerBasedProcessingController;
import edu.ksu.cis.indus.staticanalyses.tokens.ITokens;
import edu.ksu.cis.indus.staticanalyses.tokens.TokenUtil;
import edu.ksu.cis.indus.staticanalyses.tokens.soot.SootValueTypeManager;
import soot.Body;
import soot.Local;
import soot.Scene;
import soot.SootClass;
import soot.SootField;
import soot.SootMethod;
import soot.Type;
import soot.Unit;
import soot.Value;
import soot.jimple.FieldRef;
import soot.jimple.Stmt;
import soot.jimple.spark.SparkTransformer;
import soot.options.Options;

public class DiverDependenceTest extends SootBasedDriver  {
	public static String path = "";
	static boolean debugOut = false;
	private static IValueAnalyzer<Value> analyzer;
	private IndependentStmtDetector detector;
	
	public DiverDependenceTest(final IndependentStmtDetector arg) {
		detector = arg;
	}
	
	public static void main(String args[]){
		if (args.length < 1) {
			System.err.println("Too few arguments: \n\t ");
			return;
		}
		path = args[0]; // 
		if (args.length > 1) {
			debugOut = args[1].equalsIgnoreCase("-debug");
		}
		initial(path);
		enableSpark(path);
		Collection<String> classes= new ArrayList<String>();
		/* traverse all application classes */
		for (SootClass sClass:Scene.v().getApplicationClasses()) 
		{
			classes.add(sClass.toString());
		}	

		if (debugOut)
			System.out.println("classes="+classes);	
		DiverDependenceTest _dtg = new DiverDependenceTest(new IndependentStmtDetectorv2());
		_dtg.setClassNames(classes);			
		_dtg.<ITokens> execute();
	}	
	/**
	 * This contains the driver logic.
	 * 
	 * @param <T> dummy type parameter.
	 */
	private <T extends ITokens<T, Value>> void execute() {
		//setInfoLogger(LOGGER);

		final String _tagName = "AtomicityDetection:FA";
		final IValueAnalyzer<Value> _aa = OFAnalyzer.getFSOSAnalyzer(_tagName, TokenUtil
				.<T, Value, Type> getTokenManager(new SootValueTypeManager()), getStmtGraphFactory());
		final ValueAnalyzerBasedProcessingController _pc = new ValueAnalyzerBasedProcessingController();
		final Collection<IProcessor> _processors = new ArrayList<IProcessor>();
		final PairManager _pairManager = new PairManager(false, true);
		final CallGraphInfo _cgi = new CallGraphInfo(new PairManager(false, true));
		final OFABasedCallInfoCollector _callGraphInfoCollector = new OFABasedCallInfoCollector();
		final IThreadGraphInfo _tgi = new ThreadGraph(_cgi, new CFGAnalysis(_cgi, getBbm()), _pairManager);
		final ValueAnalyzerBasedProcessingController _cgipc = new ValueAnalyzerBasedProcessingController();
		final OneAllStmtSequenceRetriever _ssr = new OneAllStmtSequenceRetriever();

		_ssr.setStmtGraphFactory(getStmtGraphFactory());

		_pc.setStmtSequencesRetriever(_ssr);
		_pc.setAnalyzer(_aa);
		_pc.setProcessingFilter(new TagBasedProcessingFilter(_tagName));

		_cgipc.setStmtSequencesRetriever(_ssr);
		_cgipc.setAnalyzer(_aa);
		_cgipc.setProcessingFilter(new CGBasedProcessingFilter(_cgi));

		final Map _info = new HashMap();
		_info.put(ICallGraphInfo.ID, _cgi);
		_info.put(IThreadGraphInfo.ID, _tgi);
		_info.put(PairManager.ID, _pairManager);
		_info.put(IEnvironment.ID, _aa.getEnvironment());
		_info.put(IValueAnalyzer.ID, _aa);

		final EquivalenceClassBasedEscapeAnalysis _ecba = new EquivalenceClassBasedEscapeAnalysis(_cgi, null, getBbm());
		final IEscapeInfo _escapeInfo = _ecba.getEscapeInfo();
		_info.put(IEscapeInfo.ID, _escapeInfo);

		initialize();
		_aa.analyze(getEnvironment(), getRootMethods());

		_callGraphInfoCollector.reset();
		_processors.clear();
		_processors.add(_callGraphInfoCollector);
		_pc.reset();
		_pc.driveProcessors(_processors);
		_cgi.reset();
		_cgi.createCallGraphInfo(_callGraphInfoCollector.getCallInfo());
		writeInfo("CALL GRAPH:\n" + _cgi.toString());
		_processors.clear();
		((ThreadGraph) _tgi).reset();
		_processors.add((IProcessor) _tgi);
		_cgipc.reset();
		_cgipc.driveProcessors(_processors);
		writeInfo("THREAD GRAPH:\n" + ((ThreadGraph) _tgi).toString());
		
//        EquivalenceClassBasedEscapeAnalysis _ecba = new EquivalenceClassBasedEscapeAnalysis(_cgi, _tgi, getBbm());
//		final IReadWriteInfo _rwInfo = _ecba.getReadWriteInfo();
//		final IEscapeInfo _escapeInfo = _ecba.getEscapeInfo();
//		final AnalysesController _ac = new AnalysesController(_info, _cgipc, getBbm());
//		_ac.addAnalyses(EquivalenceClassBasedEscapeAnalysis.ID, Collections.singleton(_ecba));
//		_ac.initialize();
//		_ac.execute();
//		writeInfo("END: Escape analysis");
		
		final AnalysesController _ac = new AnalysesController(_info, _cgipc, getBbm());
		_ac.addAnalyses(EquivalenceClassBasedEscapeAnalysis.ID, Collections.singleton(_ecba));
		_ac.initialize();
		_ac.execute();
		writeInfo("END: Escape analysis");	
		//printEsscape(_ecba, _cgi);
		//System.out.println("_escapeInfo="+_escapeInfo);
		detector.setEscapeAnalysis(_escapeInfo);
		detector.hookup(_cgipc);
		_cgipc.process();
		detector.unhook(_cgipc);
		writeInfo("BEGIN: Independent region detection");

		final String _optionValue = "tag-region";  //cl.getOptionValue("scheme", "tag-stmt");

		if (_optionValue.equals("tag-region")) {
			final IndependentRegionDetector _regionDetector = new IndependentRegionDetector();
			_regionDetector.setAtomicityDetector(detector);
			_regionDetector.setBasicBlockGraphMgr(getBbm());
			_regionDetector.hookup(_cgipc);
			_cgipc.process();
			_regionDetector.unhook(_cgipc);
			insertAtomicBoundaries(_regionDetector, _cgi);
		} else {
			annotateAtomicStmts(_cgi);
		}
		writeInfo("END: Independent region detection");
//		System.out.println("path="+path);
//		dumpJimpleAndClassFiles(path, true, false);
	}
	
	// soot option 1
	private static void initial(String classPath) {
		soot.G.reset();
		Options.v().set_allow_phantom_refs(true);
		Options.v().set_process_dir(Collections.singletonList(classPath));//
		Options.v().set_whole_program(true);
		Scene.v().loadNecessaryClasses();
		
	}
	
	// soot option 2
    private static void enableSpark(String path){
        HashMap opt = new HashMap();
        //opt.put("verbose","true");
        //opt.put("propagator","worklist");
        opt.put("simple-edges-bidirectional","false");
        //opt.put("on-fly-cg","true");
        opt.put("apponly", "true");
//        opt.put("set-impl","double");
//        opt.put("double-set-old","hybrid");
//        opt.put("double-set-new","hybrid");
//        opt.put("allow-phantom-refs", "true");
        opt.put("-process-dir",path);
        
        SparkTransformer.v().transform("",opt);
    }
    
	/**
	 * Annotates the atomic statements that are in the methods reachable in given call graph.
	 * 
	 * @param cgi provides the call graph.
	 * @pre cgi != null
	 */
	private void annotateAtomicStmts(final ICallGraphInfo cgi) {
		final NamedTag _atomicTag = new NamedTag("AtomicTag");
		for (final Iterator<SootMethod> _i = cgi.getReachableMethods().iterator(); _i.hasNext();) {
			final SootMethod _sm = _i.next();
			String smStr=_sm.toString();
			if (!smStr.startsWith("<java.") && !smStr.startsWith("<sun."))
				System.out.println("\n ***************** Method: "+smStr);
			if (_sm.isConcrete()) {
				final Body _body = getBbm().getStmtGraph(_sm).getBody();
				final Collection<Unit> _sl = _body.getUnits();

				final Iterator<Unit> _j = _sl.iterator();
				final int _jEnd = _sl.size();

				for (int _jIndex = 0; _jIndex < _jEnd; _jIndex++) {
					final Stmt _stmt = (Stmt) _j.next();
					if (detector.isIndependent(_stmt)) {
						_stmt.addTag(_atomicTag);
					}
					if (!smStr.startsWith("<java.") && !smStr.startsWith("<sun."))
						System.out.println(_stmt.toString());
				}
			}
		}
	}
	
	private void insertAtomicBoundaries(final IndependentRegionDetector regionDetector, final ICallGraphInfo cgi) {
		final NamedTag _atomicBeginBeforeTag = new NamedTag("Atomic-Begin-Before-Tag");
		final NamedTag _atomicBeginAfterTag = new NamedTag("Atomic-Begin-After-Tag");
		final NamedTag _atomicEndBeforeTag = new NamedTag("Atomic-End-Before-Tag");
		final NamedTag _atomicEndAfterTag = new NamedTag("Atomic-End-After-Tag");

		for (final Iterator _i = cgi.getReachableMethods().iterator(); _i.hasNext();) {
			final SootMethod _sm = (SootMethod) _i.next();

			for (final Iterator _j = regionDetector.getAtomicRegionBeginBeforeBoundariesFor(_sm).iterator(); _j.hasNext();) {
				final Stmt _stmt = (Stmt) _j.next();
				_stmt.addTag(_atomicBeginBeforeTag);
			}

			for (final Iterator _j = regionDetector.getAtomicRegionBeginAfterBoundariesFor(_sm).iterator(); _j.hasNext();) {
				final Stmt _stmt = (Stmt) _j.next();
				_stmt.addTag(_atomicBeginAfterTag);
			}

			for (final Iterator _j = regionDetector.getAtomicRegionEndBeforeBoundariesFor(_sm).iterator(); _j.hasNext();) {
				final Stmt _stmt = (Stmt) _j.next();
				_stmt.addTag(_atomicEndBeforeTag);
			}

			for (final Iterator _j = regionDetector.getAtomicRegionEndAfterBoundariesFor(_sm).iterator(); _j.hasNext();) {
				final Stmt _stmt = (Stmt) _j.next();
				_stmt.addTag(_atomicEndAfterTag);
			}
		}
	}
	
    private static void printEsscape(EquivalenceClassBasedEscapeAnalysis _ecba, ICallGraphInfo _cgi){
		final IReadWriteInfo _rwInfo = _ecba.getReadWriteInfo();
		final IEscapeInfo _escapeInfo = _ecba.getEscapeInfo();
		System.out.println("ReadWrite-Effect and Escape Information:");
		final String[] _emptyStringArray = new String[0];

		for (final Iterator<SootMethod> _i = _cgi.getReachableMethods().iterator(); _i.hasNext();) {
			final SootMethod _sm = _i.next();
			System.out.println("Method: " + _sm.getSignature());
			System.out.println("\tsealed: " + _ecba.isMethodSealed(_sm) + ", atomic: " + _ecba.isMethodAtomic(_sm));
			if (!_sm.isStatic()) {
				System.out.println("\tthis:");
				System.out.println("\t\tread =  " + _rwInfo.isThisBasedAccessPathRead(_sm, _emptyStringArray, true));
				System.out.println("\t\twritten =  " + _rwInfo.isThisBasedAccessPathWritten(_sm, _emptyStringArray, true));
				System.out.println("\t\tescapes = " + _escapeInfo.thisEscapes(_sm));
				System.out.println("\t\tmulti-thread RW = "
						+ _escapeInfo.thisFieldAccessShared(_sm, IEscapeInfo.READ_WRITE_SHARED_ACCESS));
				System.out.println("\t\tmulti-thread LU = " + _escapeInfo.thisLockUnlockShared(_sm));
				System.out.println("\t\tmulti-thread WN = " + _escapeInfo.thisWaitNotifyShared(_sm));
				System.out.println("\t\tfield reading threads = " + _escapeInfo.getReadingThreadsOfThis(_sm));
				System.out.println("\t\tfield writing threads = " + _escapeInfo.getWritingThreadsOfThis(_sm));

				for (final Iterator<SootField> _j = _sm.getDeclaringClass().getFields().iterator(); _j.hasNext();) {
					SootField _field = _j.next();
					if (!_field.isStatic()) {
						final String[] _accessPath = { _field.getSignature() };
						System.out.println("\t\t\t" + _accessPath[0] + ": [read: "
								+ _rwInfo.isThisBasedAccessPathRead(_sm, _accessPath, true) + ", written:"
								+ _rwInfo.isThisBasedAccessPathWritten(_sm, _accessPath, true) + "]");
					}
				}
				System.out.print("\t\tcoupled with parameters at position: ");
				for (int _k = 0; _k < _sm.getParameterCount(); _k++) {
					if (EquivalenceClassBasedEscapeAnalysis.canHaveAliasSet(_sm.getParameterType(_k))
							&& _rwInfo.canMethodInduceSharingBetweenParamAndThis(_sm, _k)) {
						System.out.print(_k + ", ");
					}
				}
				System.out.println("");
			}

			for (int _j = 0; _j < _sm.getParameterCount(); _j++) {
				System.out.println("\tParam" + (_j + 1) + "[" + _sm.getParameterType(_j) + "]:");
				System.out.println("\t\tread = " + _rwInfo.isParameterBasedAccessPathRead(_sm, _j, _emptyStringArray, true));
				System.out.println("\t\twritten = "
						+ _rwInfo.isParameterBasedAccessPathWritten(_sm, _j, _emptyStringArray, true));
				System.out.println("\t\tfield reading threads: " + _escapeInfo.getReadingThreadsOf(_j, _sm));
				System.out.println("\t\tfield writing threads: " + _escapeInfo.getWritingThreadsOf(_j, _sm));
				System.out.print("\t\tcoupled with parameters at position: ");
				for (int _k = 0; _k < _sm.getParameterCount(); _k++) {
					if (_k != _j && EquivalenceClassBasedEscapeAnalysis.canHaveAliasSet(_sm.getParameterType(_k))
							&& EquivalenceClassBasedEscapeAnalysis.canHaveAliasSet(_sm.getParameterType(_j))
							&& _rwInfo.canMethodInduceSharingBetweenParams(_sm, _j, _k)) {
						System.out.print(_k + ", ");
					}
				}
				System.out.println("");

			}

			if (_sm.hasActiveBody()) {
				final Body _body = _sm.getActiveBody();

				for (final Iterator<Local> _j = _body.getLocals().iterator(); _j.hasNext();) {
					final Local _local = _j.next();
					System.out.println("\tLocal " + _local.getName() + "[" + _local.getType() + "] : ");
					System.out.println("\t\tescapes = " + _escapeInfo.escapes(_local, _sm));
					System.out.println("\t\tshared RW = "
							+ _escapeInfo.fieldAccessShared(_local, _sm, IEscapeInfo.READ_WRITE_SHARED_ACCESS));
					System.out.println("\t\tshared WW = "
							+ _escapeInfo.fieldAccessShared(_local, _sm, IEscapeInfo.WRITE_WRITE_SHARED_ACCESS));
					System.out.println("\t\tlocking = " + _escapeInfo.lockUnlockShared(_local, _sm));
					System.out.println("\t\twaitNotify  = " + _escapeInfo.waitNotifyShared(_local, _sm));
					System.out.println("\t\tfield reading threads: " + _escapeInfo.getReadingThreadsOf(_local, _sm));
					System.out.println("\t\tfield writing threads: " + _escapeInfo.getWritingThreadsOf(_local, _sm));
				}

				for (final Iterator<Unit> _j = _body.getUnits().iterator(); _j.hasNext();) {
					final Stmt _stmt = (Stmt) _j.next();
					if (_stmt.containsFieldRef()) {
						final FieldRef _fr = _stmt.getFieldRef();
						SootField _field = _fr.getField();
						if (_field.isStatic()) {
							System.out.println("\tStatic " + _field.getSignature() + " : ");
							System.out.println("\t\tshared RW = "
									+ _escapeInfo.staticfieldAccessShared(_field.getDeclaringClass(), _sm, _field
											.getSignature(), IEscapeInfo.READ_WRITE_SHARED_ACCESS));
							System.out.println("\t\tshared WW = "
									+ _escapeInfo.staticfieldAccessShared(_field.getDeclaringClass(), _sm, _field
											.getSignature(), IEscapeInfo.WRITE_WRITE_SHARED_ACCESS));
						}
					}
				}
			}
		}        
    }
}